export const cards = [
    {
        id: 1,
        section: 'imgProc',
        operations: ['experiment', 'learning'],
        name: '图像处理',
        description: '包含「实验」和「学习」两部分，「实验」支持"自定义处理流程"，进行实验，「学习」可进行原理学习',
        actions: ['实验', '学习'],
    },
];
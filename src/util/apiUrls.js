export default {
    getAllOperations: process.env.NODE_ENV === 'production' ? '/apis/imgproc/lab' : '/imgproc/lab',
    sendOperation: process.env.NODE_ENV === 'production' ? '/apis/imgproc/lab' : '/imgproc/lab',
}